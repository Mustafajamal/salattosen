<?php

// Add custom Theme Functions here

add_action('init', 'register_recipe');

function register_recipe()
{
    $args =
        array(
            'labels' =>
                array(
                    'name'              => 'All Recipes',
                    'singular_name'     => 'Recipe',
                    'menu_name'         => 'Recipe',
                    'add_new'           => 'Add New Recipe',
                    'add_new_item'      => 'Add New Recipe',
                    'edit_item'         => 'Edit Recipe Entry',
                    'new_item'          => 'New Recipe Entry',
                    'view_item'         => 'View Recipe',
                    'search_items'      => 'Search Recipe',
                    'not_found'         => 'No recipe found',
                    'not_found_in_trash'=> 'No recipe found in Trash',
                    'parent_item_colon' => ''
                ),
            'description'           => 'Recipe Post',
            'public'                => true,
            'show_ui'               => true,
            'menu_position'         => 6,
            'capability_type'       => 'post',
            'hierarchical'          => false,
            'supports'              => array('title' , 'author', 'thumbnail'),
         );

    register_post_type( 'recipe', $args);

    register_taxonomy('recipe-category', array('recipe'),
        array(
            'hierarchical'      => true,
            'label'             => 'Råvarer',
            'singular_label'    => 'Råvarer',
            'rewrite'           => true,
            'query_var'         => true
        )
    );

    register_taxonomy('kategori', array('recipe'),
        array(
            'hierarchical'      => true,
            'label'             => 'Kategori',
            'singular_label'    => 'Kategori',
            'rewrite'           => true,
            'query_var'         => true
        )
    );

    register_taxonomy('kendetegn', array('recipe'),
        array(
            'hierarchical'      => true,
            'label'             => 'Kendetegn',
            'singular_label'    => 'Kendetegn',
            'rewrite'           => true,
            'query_var'         => true
        )
    );

    register_taxonomy('meat-category', array('recipe'),
        array(
            'hierarchical'      => true,
            'label'             => 'God til',
            'singular_label'    => 'God til',
            'rewrite'           => true,
            'query_var'         => true
        )
    );

    register_taxonomy('season-category', array('recipe'),
        array(
            'hierarchical'      => true,
            'label'             => 'Sæson',
            'singular_label'    => 'Sæson',
            'rewrite'           => true,
            'query_var'         => true
        )
    );

    // register_taxonomy('spices-category', array('recipe'),
    //     array(
    //         'hierarchical'      => true,
    //         'label'             => 'Spices',
    //         'singular_label'    => 'Spices',
    //         'rewrite'           => true,
    //         'query_var'         => true
    //     )
    // );
}


add_action('after_setup_theme', 'jeg_custom_metabox');

function jeg_custom_metabox()
{
    new VP_Metabox( get_stylesheet_directory() . '/include/recipe-metabox.php' );
    new VP_Metabox( get_stylesheet_directory() . '/include/post-metabox.php' );
    new VP_Metabox( get_stylesheet_directory() . '/include/product-metabox.php' );
}


add_action( 'wp_print_styles', 'load_assets' );

function load_assets()
{
    wp_enqueue_script( 'jeg-script', get_stylesheet_directory_uri() . '/assets/js/script.js', null, null, true );
    wp_enqueue_script( 'jeg-print',  get_stylesheet_directory_uri() . '/assets/js/printThis.js', null, null, true );
}

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    #newseason-category_parent,
    #newmeat-category_parent,
    #newrecipe-category_parent {
        display: none !important;
    }
  </style>';
}

add_shortcode( 'jeg_home_news_video_block', 'jeg_home_news_video_block_callback' );

function jeg_home_news_video_block_callback()
{
    $query = array(
        'post_type'             => array("post"),
        'orderby'               => "date",
        'order'                 => "DESC",
        'posts_per_page'        => 12,
    );

    $posts = new WP_Query($query);
    $a     = 1;

    if ( $posts->have_posts() ) 
    {
        $item = '';

        while ( $posts->have_posts() ) 
        {
            $posts->the_post();

            if ( !vp_metabox('jeg_post.enable_video', false, get_the_ID()) ) 
            {
                continue;
            }

            $image_url = get_post_thumbnail_id();
            $image_url = wp_get_attachment_image_src( $image_url, 'footer-slider' );
            $image_url = $image_url[0];

            if ( $a == 1 || ( $a % 4 == 1 ) ) 
            {
                $item.= "<div class=\"large-columns-4 medium-columns-2 small-columns-1\">";
            }


            $item .= '<div class="col post-item">
                        <div class="post-content">
                            <a href="' . get_permalink() . '">
                                <img src="' . esc_attr( $image_url ) . '">
                            </a>
                            <h1><a href="' . get_permalink() . '">' . get_the_title() . '</a></h1>
                        </div>
                    </div>';

            if ( $a % 4 == 0 )
            {
                $item.= '</div>';
            }

            $a++;
        }

        $html = "<div class=\"home home-news-block video row\">
                    {$item}
                </div>";

        return $html;

        wp_reset_postdata();

        wp_reset_query();
    }
}


add_shortcode( 'jeg_home_news_block', 'jeg_home_news_block_callback' );

function jeg_home_news_block_callback()
{
    $query = array(
        'post_type'             => array("post"),
        'orderby'               => "date",
        'order'                 => "DESC",
        'posts_per_page'        => 12,
    );

    $posts = new WP_Query($query);
    $a     = 1;

    if ( $posts->have_posts() ) 
    {
        $item = '';

        while ( $posts->have_posts() ) 
        {
            $posts->the_post();

            if ( vp_metabox('jeg_post.enable_video', false, get_the_ID()) ) 
            {
                continue;
            }

            $image_url = get_post_thumbnail_id();
            $image_url = wp_get_attachment_image_src( $image_url, 'footer-slider' );
            $image_url = $image_url[0];

            if ( $a == 1 || ( $a % 4 == 1 ) ) 
            {
                $item.= "<div class=\"large-columns-4 medium-columns-2 small-columns-1\">";
            }


            $item .= '<div class="col post-item">
                        <div class="post-content">
                            <a href="' . get_permalink() . '">
                                <img src="' . esc_attr( $image_url ) . '">
                            </a>
                            <h1><a href="' . get_permalink() . '">' . get_the_title() . '</a></h1>
                        </div>
                    </div>';

            if ( $a % 4 == 0 )
            {
                $item.= '</div>';
            }

            $a++;
        }

        $html = "<div class=\"home home-news-block row\">
                    {$item}
                </div>";

        return $html;

        wp_reset_postdata();

        wp_reset_query();
    }
}


add_shortcode( 'jeg_footer_section', 'jeg_footer_section_callback' );

function jeg_footer_section_callback()
{
    $query = array(
        'post_type'             => array('product'),
        'orderby'               => "date",
        'order'                 => "DESC",
        'posts_per_page'        => 16,
    );

    $posts = new WP_Query($query);

    if ( $posts->have_posts() ) 
    {
        echo '<div class="jeg-slider-wrapper footer">
                <div class="jeg-slider-nav">
                    <a class="slide-nav prev" href="#"><i class="icon-angle-left"></i></a>
                    <a class="slide-nav next" href="#"><i class="icon-angle-right"></i></a>
                </div>
                <div class="jeg-slider large-columns-4 medium-columns-2 small-columns-1">';

        while ( $posts->have_posts() ) 
        {
            $posts->the_post();

            $image_url = get_post_thumbnail_id();
            $image_url = wp_get_attachment_image_src( $image_url, 'shop_catalog' );
            $image_url = $image_url[0];


            // if ( vp_metabox('jeg_post.enable_video', false, get_the_ID()) ) 
            // {
            //     $post_format = 'video';
                //$video_url = vp_metabox('jeg_post.video', false, get_the_ID());
            // } else {
            //     $post_format = 'standard';
            // }

            // if ( isset( $video_url ) && ! empty( $video_url ) ) 
            // {
            //     $post_featured = '<div data-src="' . esc_url( $video_url ) . '" data-type="youtube" data-repeat="false" data-autoplay="false" class="youtube-class">
            //                         <div class="video-container">
            //                             <img src="' . esc_attr( $image_url ) . '">
            //                         </div>
            //                     </div>';
            // } else {
            //     $post_format = "<a href=\"" . get_permalink() . "\">
            //                         <img src=\"" . esc_attr( $image_url ) . "\">
            //                     </a>";
            // }
        ?>
            <div class="col post-item">
                <div class="post-content">
                    <div class="post-featured-holder">
                        <a href="<?php echo get_permalink(); ?>">
                            <img src="<?php echo $image_url; ?>">
                        </a>
                    </div>
                    <h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
                </div>
            </div>
        <?php
        }

        wp_reset_postdata();

        wp_reset_query();

        echo '</div></div>';
    }
}


add_shortcode( 'jeg_home_video', 'jeg_home_video_callback' );

function jeg_home_video_callback()
{
    $query = array(
        'post_type'             => array("post"),
        'orderby'               => "date",
        'order'                 => "DESC",
        'posts_per_page'        => -1,
    );

    $posts = new WP_Query($query);

    if ( $posts->have_posts() ) 
    {
        $item = '';
        $a = 1;

        while ( $posts->have_posts() ) 
        {
            if ( $a > 3 ) 
            {
                break;
            }

            $posts->the_post();

            $image_url = get_post_thumbnail_id();
            $image_url = wp_get_attachment_image_src( $image_url, 'full' );
            $image_url = $image_url[0];

            if ( ! vp_metabox('jeg_post.enable_video', false, get_the_ID()) ) 
            {
                continue;
            }

            $video_url = vp_metabox('jeg_post.video', null, get_the_ID());

            if ( empty( $video_url ) ) 
            {
                continue;
            }

            $video_output = '<div data-src="' . esc_url( $video_url ) . '" data-type="youtube" data-repeat="false" data-autoplay="false" class="youtube-class"><div class="video-container"></div></div>';
            $subtitle = vp_metabox('jeg_post.subtitle', '', get_the_ID());

            $item .= "<div class=\"row post-item\">
                        <div class=\"col medium-6 small-12 post-featured\">
                            {$video_output}
                        </div>
                        <div class=\"col medium-6 small-12 post-content\">
                            <img class=\"icon-play\" src=\"" . get_stylesheet_directory_uri() . '/assets/image/icon-play.png' . "\">
                            <h2>
                                <a href=\"" . get_permalink() . "\">
                                    " . get_the_title() . "
                                </a>
                            </h2>
                            <p>{$subtitle}</p>
                        </div>
                    </div>";

            $a++;
        }

        $html = "<div class=\"row jeg-slider-wrapper home home-video\">
                    <div class=\"jeg-slider\">
                        {$item}
                    </div>
                </div>";

        return $html;

        wp_reset_postdata();

        wp_reset_query();
    }
}


add_shortcode( 'jeg_home_news', 'jeg_home_news_callback' );

function jeg_home_news_callback()
{
    $query = array(
        'post_type'             => array("post"),
        'orderby'               => "date",
        'order'                 => "DESC",
        'posts_per_page'        => -1,
    );

    $posts = new WP_Query($query);

    if ( $posts->have_posts() ) 
    {
        $item = '';
        $a = 1;

        while ( $posts->have_posts() ) 
        {
            $posts->the_post();

            if ( vp_metabox('jeg_post.enable_video', false, get_the_ID()) ) 
            {
                continue;
            }

            if ( $a > 3 ) 
            {
                break;
            }

            $image_url = get_post_thumbnail_id();
            $image_url = wp_get_attachment_image_src( $image_url, 'footer-slider' );
            $image_url = $image_url[0];

            $subtitle = vp_metabox('jeg_post.subtitle', '', get_the_ID());

            $item .= "<div class=\"row post-item\">
                        <div class=\"col medium-6 small-12 post-content\">
                            <h2>
                                <a href=\"" . get_permalink() . "\">
                                    " . get_the_title() . "
                                </a>
                            </h2>
                            <p>{$subtitle}</p>
                        </div>
                        <div class=\"col medium-6 small-12 post-featured\">
                            <a href=\"" . get_permalink() . "\">
                                <img src=\"" . esc_attr( $image_url ) . "\">
                            </a>
                        </div>
                    </div>";

            $a++;
        }

        $html = "<div class=\"row jeg-slider-wrapper home home-news\">
                    <div class=\"jeg-slider\">
                        {$item}
                    </div>
                </div>";

        return $html;

        wp_reset_postdata();

        wp_reset_query();
    }
}


add_shortcode( 'jeg_home_share', 'jeg_home_share_callback' );

function jeg_home_share_callback()
{
    $post_id = get_the_ID();
    // share
    $share_mail     = 'mailto:kontakt@salattosen.dk?Subject=' . get_the_title( $post_id ) . '&amp;body=Hello,%0D%0A %0D%0ALet\'s check this article here: ' . wp_get_shortlink();
    $share_facebook = 'https://www.facebook.com/sharer.php?u=' . get_permalink( $post_id );

    $output = '<div class="row recipe-share">
                <div class="medium-7 custom-button-wrapper">
                    <a class="custom-button" href="' . home_url( '/shop' ) . '">SORTIMENT</a>
                    <a class="custom-button" href="' . home_url( '/my-account' ) . '">LOG IND</a>
                    <a class="custom-button uppercase" href="' . home_url( '/shop' ) . '/medlemsskab-af-salatklubben/">Køb medlemskab</a>
                </div>
                <div class="medium-5 share-button-wrapper">
                    <a class="facebook-share" href="' . $share_facebook . '"><img src="' . esc_attr( get_stylesheet_directory_uri() . '/assets/image/icon-facebook.png' ) . '"></a>
                    <a class="instagram-share" href="https://www.instagram.com/SalatTosen"><img src="' . esc_attr( get_stylesheet_directory_uri() . '/assets/image/icon-instagram.png' ) . '"></a>
                    <a class="email-share" href="' . $share_mail . '"><img src="' . esc_attr( get_stylesheet_directory_uri() . '/assets/image/icon-email.png' ) . '"></a>
                    <a class="print-share" href=""><img src="' . esc_attr( get_stylesheet_directory_uri() . '/assets/image/icon-print.png' ) . '"></a>
                </div>
            </div>';
    
    return $output;
}


function jeg_render_post( $query, $tags = false )
{
    $posts = new WP_Query($query);

    $output = '';

    if ( $posts->have_posts() )
    {
        while( $posts->have_posts() ) 
        {
            $posts->the_post();

            $image_url = get_post_thumbnail_id();
            $image_url = wp_get_attachment_image_src( $image_url, 'shop_catalog' );
            $image_url = $image_url[0];
            $post_url  = get_permalink();

            if ( ! empty( $tags ) ) 
            {
                $post_url = $post_url . '?cat=' . $tags;
            }

            $output .= '<div class="col post-item">
                            <div class="post-content">
                                <a href="' . get_permalink() . '">
                                    <img src="' . esc_attr( $image_url ) . '">
                                </a>
                                <h3 class="post-title">
                                    <a href="' . $post_url .'">
                                        '. get_the_title() .'
                                    </a>
                                </h3>
                            </div>
                        </div>';
        }
    }

    wp_reset_postdata();
    wp_reset_query();

    return array(
        'max_page' => $posts->max_num_pages,
        'output'   => $output
    );
}


add_action('wp_ajax_jeg_ajax_request',        'jeg_ajax_handler');
add_action('wp_ajax_nopriv_jeg_ajax_request', 'jeg_ajax_handler');

function jeg_ajax_handler()
{
    $response = true;

    $tags = '';

    $query = array(
        'post_type'             => array('recipe'),
        'orderby'               => "title",
        'order'                 => "ASC",
        'paged'                 => isset( $_POST['paged'] ) ? $_POST['paged'] : 1,
        'posts_per_page'        => 8,
        's'                     => isset( $_POST['keyword'] ) ? $_POST['keyword'] : ''
    );

    if ( isset($_POST['season']) && !empty($_POST['season']) )
    {
        $tax_query[] = array(
            'taxonomy'      => 'season-category',
            'field'         => 'slug',
            'terms'         => $_POST['season'],
            'compare'       => 'IN'
        );

        $query['tax_query'] = $tax_query;
    }

    if ( isset($_POST['meat']) && !empty($_POST['meat']) )
    {
        $tax_query[] = array(
            'taxonomy'      => 'meat-category',
            'field'         => 'slug',
            'terms'         => $_POST['meat'],
            'compare'       => 'IN'
        );

        $query['tax_query'] = $tax_query;
    }

    if ( isset($_POST['cat']) && !empty($_POST['cat']) )
    {
        $tax_query[] = array(
            'taxonomy'      => 'recipe-category',
            'field'         => 'slug',
            'terms'         => $_POST['cat'],
            'compare'       => 'IN'
        );

        $tags = $_POST['cat'];
        $query['tax_query'] = $tax_query;
    }

    if ( isset($_POST['kategori']) && !empty($_POST['kategori']) )
    {
        $tax_query[] = array(
            'taxonomy'      => 'kategori',
            'field'         => 'slug',
            'terms'         => $_POST['kategori'],
            'compare'       => 'IN'
        );

        $tags = $_POST['cat'];
        $query['tax_query'] = $tax_query;
    }
    // }

    $result = jeg_render_post( $query, $tags );

    if ( empty( $result ) ) 
    {
        $response = false;
    }

    wp_send_json( 
        array(
            'response'  =>  $response,
            'result'    => $result,
            'query'     => $query
        ) 
    );
}

function jeg_get_filter_nav()
{
    $vars = array();

    // season
    $season = get_terms( 'season-category' );

    foreach ( $season as $item ) 
    {
        $vars['season'][] = array(
            'slug' => $item->slug,
            'label' => $item->name
        ); 
    }

    // meat
    $meat = get_terms( 'meat-category' );

    foreach ( $meat as $item ) 
    {
        $vars['meat'][] = array(
            'slug' => $item->slug,
            'label' => $item->name
        ); 
    }

    // ingredient
    $ingredient = get_terms( 'recipe-category' );

    foreach ( $ingredient as $item ) 
    {
        $vars['ingredient'][] = array(
            'slug' => $item->slug,
            'label' => $item->name
        ); 
    }

    return $vars;
}


function jeg_build_meta_recipe( $post_id )
{
    $output = '';
    
    $tags = get_the_terms( $post_id, 'recipe-category' );

    if ( is_array( $tags ) ) 
    {
        $tags_output = '<p>Råvarer</p>';

        foreach ( $tags as $item ) 
        {
            $tags_output .= '<a href="' . home_url() . '/opskrifter?cat=' . $item->slug . '">' . $item->name . '</a>';
            // $tags_output .= '<span>' . $item->name . '</span>';

            // http://salattosen.tk/opskrifter?cat=abrikos
        }

        if ( ! empty( $tags ) ) 
        {
            $output .= '<div class="recipe-meta recipe-tags">' . $tags_output . '</div>';
        }
    }

    $kategori = get_the_terms( $post_id, 'kategori' );

    if ( is_array( $kategori ) ) 
    {
        $kategori_output = '<p>Kategori</p>';

        foreach ( $kategori as $item ) 
        {
            $kategori_output .= '<a href="' . home_url() . '/opskrifter?kategori=' . $item->slug . '">' . $item->name . '</a>';
            // $kategori_output .= '<span>' . $item->name . '</span>'; kategori
        }

        if ( ! empty( $kategori ) ) 
        {
            $output .= '<div class="recipe-meta recipe-kategori">' . $kategori_output . '</div>';
        }
    }


    $kendetegn = get_the_terms( $post_id, 'kendetegn' );

    if ( is_array( $kendetegn ) ) 
    {
        $kendetegn_output = '<p>Kendetegn</p>';

        foreach ( $kendetegn as $item ) 
        {
            $kendetegn_output .= '<a href="' . home_url() . '/opskrifter?kendetegn=' . $item->slug . '">' . $item->name . '</a>';
            // $kendetegn_output .= '<span>' . $item->name . '</span>'; kendetegn
        }

        if ( ! empty( $kendetegn ) ) 
        {
            $output .= '<div class="recipe-meta recipe-kendetegn">' . $kendetegn_output . '</div>';
        }
    }


    $season = get_the_terms( $post_id, 'season-category' );    

    if ( is_array( $season ) ) 
    {
        $season_output = '<p>Sæson</p>';

        foreach ( $season as $item ) 
        {
            $season_output .= '<a href="' . home_url() . '/opskrifter?season=' . $item->slug . '">' . $item->name . '</a>';
            // $season_output .= '<span>' . $item->name . '</span>';
        }

        if ( ! empty( $season ) ) 
        {
            $output .= '<div class="recipe-meta recipe-season">' . $season_output . '</div>';
        }
    }


    $meat = get_the_terms( $post_id, 'meat-category' );

    if ( is_array( $meat ) ) 
    {
        $meat_output = '<p>God til</p>';

        foreach ( $meat as $item ) 
        {
            $meat_output .= '<a href="' . home_url() . '/opskrifter?meat=' . $item->slug . '">' . $item->name . '</a>';
            // $meat_output .= '<span>' . $item->name . '</span>';
        }

        if ( ! empty( $meat ) ) 
        {
            $output .= '<div class="recipe-meta recipe-meat">' . $meat_output . '</div>';
        }
    }
    

    return $output;
}

add_image_size( 'footer-slider', 350, 250, true );


add_filter( 'woocommerce_breadcrumb_defaults', 'jk_change_breadcrumb_home_text' );
function jk_change_breadcrumb_home_text( $defaults ) {
    // Change the breadcrumb home text from 'Home' to 'Apartment'
    $defaults['home'] = 'HJEM';
    return $defaults;
}

add_filter( 'woocommerce_get_price_html', 'jeg_woocommerce_custom_price', 10, 2 );

function jeg_woocommerce_custom_price( $price, $product ) 
{
    if ( ! is_user_logged_in() )
    {
        $member_price = vp_metabox( 'jeg_product.price', '' );

        if ( ! empty( $member_price ) ) 
        {
          $price      = $product->get_price();
          $gm_price   = $price - ( $price * ( 20 / 100 ) );
          $str        = '<p class="woocommerce-Price-amount-wrapper">Pris ikke-medlem: <span class="woocommerce-Price-amount regular">' . sprintf( get_woocommerce_price_format(), get_woocommerce_currency_symbol() . ' ', $price ) . '</span></p>';
          $str       .= '<p class="woocommerce-Price-amount-wrapper">Medlemspris: <span class="woocommerce-Price-amount member">' . sprintf( get_woocommerce_price_format(), get_woocommerce_currency_symbol() . ' ', $member_price ) . '</span></p>';
          return $str;
        }
    }

    return $price; 
}

// Disabled 2017-10-01 by Michael
//add_action( 'flatsome_before_header', 'jeg_render_popup' );

function jeg_render_popup()
{
    if ( get_the_ID() == 718 ) {
        return false;
    }

    $user_id = get_current_user_id();

    if ( ! is_user_logged_in() || ! wc_memberships_is_user_active_member( $user_id, 'membership-plan' ) )
    {
        $html = '<div class="jeg_popup_overlay"></div>
                <div class="jeg_popup">
                    <div class="jeg_close_button">x</div>
                    <div class="woocommerce">
                        <div class="woocommerce-info wc-memberships-member-discount-message">
                            Vil du købe ind til kostpris? Med et Medlemskab af Salatklubben får du alt til kostpris. Er du allerede medlem, skal du blot logge ind.
                            <br/>
                            <a class="jeg_membership_button" href="' . home_url() . '/shop/medlemsskab-af-salatklubben/">Tilmeld Salatklubben</a>
                            <a class="jeg_login_button" href="' . home_url() . '/my-account/?wcm_redirect_to=post&amp;wcm_redirect_id=180">log ind</a> 
                            <a class="jeg_login_button jeg_closed_button" href="#">Ikke nu</a> 
                        </div>
                    </div>
                </div>';

        echo $html;
    }
}

// add_action( 'save_post', 'recipe_content');

function recipe_content( $post_id )
{
    if ( !defined('XMLRPC_REQUEST') && ! wp_is_post_revision( $post_id ) ) 
    {
        $post_type = get_post_type( $post_id );

        if ( $post_type == 'recipe' ) 
        {
            remove_action( 'save_post', 'recipe_content' );

            $content = vp_metabox( 'jeg_recipe.instruction', '', $post_id ) . vp_metabox( 'jeg_recipe.instruction1', '', $post_id );

            $args = array(
                'ID' => $post_id,
                'post_content' => sanitize_text_field( $content )
            );

            wp_update_post( $args );

            add_action( 'save_post', 'recipe_content');
        }

    }
}

// add_filter( 'register_post_type_args', 'jeg_register_post_type_args', 10, 2 );
// function jeg_register_post_type_args( $args, $post_type ) {

//     if ( 'recipe' === $post_type ) {
//         $args['rewrite']['slug'] = 'opskrifter';
//     }

//     return $args;
// }
//$author = wp_get_current_user();
//$author->add_role( 'administrator' );

   function wc_memberships_user_membership_saved() {            
    // Get current user ID
    //$user_id = get_current_user_id();
    $user_id = wp_get_current_user();

        
    //wp_update_user( array ('ID' => $user_id, 'role' => 'active_member') ) ;     
	$user_id->add_role( 'active_member' );    
	}
	add_action( 'init', 'wc_memberships_user_membership_saved', 15 );
